// Dom CSS 
document.querySelector(".main").style.background = "#EDF2F9";
document.querySelector(".main-1").style.background = "#ECF0F3";
document.querySelector(".main-1").style.minHeight = "80vh"
document.querySelector(".main-1").style.boxShadow = "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px"

// Add element in array and display them to page
var numArr = []
var floatArr = []
function addElement(){
    number = document.querySelector("#number").value*1;
    numArr.push(number);
    document.querySelector("#number").value = ""
    document.querySelector(".main-result").innerHTML = `${numArr}`
}
function addFloatElement(){
    number = document.querySelector("#float").value*1;
    floatArr.push(number);
    document.querySelector("#float").value = ""
    document.querySelector(".extra-result").innerHTML = `${floatArr}`
}

// Sum of all elements
function tinhTong(){
    var tong = 0;
    numArr.forEach(function(element){
        tong += element;
    })
    document.querySelector(".main-result-1").innerHTML = `${tong}`
}

// Đếm số dương
function demSoDuong(){
    var count = 0;
    for(var i=0; i<numArr.length; i++){
        if(numArr[i] > 0) count++;
    }
    document.querySelector(".main-result-2").innerHTML = `${count}`;
}

// Find the minimum value of element in array
function findMin(){
    var min = numArr[0];
    for(var i=1; i<numArr.length; i++){
        if(min > numArr[i]) min = numArr[i];
    }
    document.querySelector(".main-result-3").innerHTML = `${min}`;
}

// Tìm số dương nhỏ nhất
function timSoDuongNhoNhat(){
    var start = 0;
    var minDuong = -1;
    for(;start<numArr.length; start++){
        if(numArr[start] > 0){
            minDuong = numArr[start];
            break;
        }
    }
    for(var i=start; i<numArr.length; i++){
        if(minDuong > numArr[i] && numArr[i] > 0){
            minDuong = numArr[i];
        }
    }
    if(minDuong == -1){
        document.querySelector(".main-result-4").innerHTML = `Mảng của bạn không có số dương`;
    }
    else{
        document.querySelector(".main-result-4").innerHTML = `${minDuong}`;
    }
}

// Tìm số chẵn cuối cùng
function findLastEvenNumber(){
    var lastEven = -1;
    numArr.forEach(function(element){
        if(element%2 == 0) lastEven = element; 
    })
    document.querySelector(".main-result-5").innerHTML = `${lastEven}`;
}

// Swap element according to index
function swapElement(){
    var copyArr = [];
    for(var i = 0; i<numArr.length; i++){
        copyArr[i] = numArr[i]
    }
    var index1 = document.querySelector("#number-1").value*1;
    var index2 = document.querySelector("#number-2").value*1;
    var temp = copyArr[index1];
    copyArr[index1] = copyArr[index2];
    copyArr[index2] = temp;
    document.querySelector(".main-result-6").innerHTML = `Mảng sau khi đổi chỗ: ${copyArr}`
}

// Sort
function sortArray(){
    // bubblesort
    for(var i=0; i<numArr.length-1; i++){
        for(var j=i+1; j<numArr.length; j++){
            if(numArr[i] > numArr[j]){
                var temp = numArr[i];
                numArr[i] = numArr[j];
                numArr[j] = temp;
            }
        }
    }
    document.querySelector(".main-result-7").innerHTML = `Mảng sau khi sắp xếp: ${numArr}`
}

// Find the first element is a prime
function checkPrime(n){
    if(n <= 1) return false;
    for(var i=2; i<=n/2; i++){
        if(n%i == 0) return false;
    }
    return true;
}
function findFirstPrime(){
    var result = -1;
    for(var i=0; i<numArr.length; i++){
        if(checkPrime(numArr[i])){
            result = numArr[i];
            break;
        } 
    }
    document.querySelector(".main-result-8").innerHTML = `${result}`;
}

// Đếm số nguyên
function demSoNguyen(){
    var count = 0;
    for(var i=0; i<floatArr.length; i++){
        if(Number.isInteger(floatArr[i])) count++; 
    }
    document.querySelector(".main-result-9").innerHTML = `Số nguyên: ${count}`;
}

// So sánh số âm và số dương
function soSanh(){
    var countAm = 0;
    var countDuong = 0;
    numArr.forEach(function(element){
        if(element > 0) countDuong++;
        if(element < 0) countAm++;
    })
    if(countAm == countDuong){
        document.querySelector(".main-result-10").innerHTML = `Số âm = Số dương`
    }
    else if(countAm > countDuong){
        document.querySelector(".main-result-10").innerHTML = `Số âm > Số dương`
    }
    else{
        document.querySelector(".main-result-10").innerHTML = `Số âm < Số dương`
    }
}



